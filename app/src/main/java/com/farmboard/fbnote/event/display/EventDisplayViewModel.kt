package com.farmboard.fbnote.event.display

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.database.EventDatabaseDao

class EventDisplayViewModel(
    eventDB: EventDatabaseDao,
    eventId: String,
    application: Application
) : AndroidViewModel(application) {

    private val _event = eventDB.getSingle(eventId)

    val event: LiveData<Event?>
        get() = _event
}
