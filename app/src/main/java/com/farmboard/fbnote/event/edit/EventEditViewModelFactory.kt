package com.farmboard.fbnote.event.edit

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.database.EventDatabaseDao
import java.io.File

/**
 * This is pretty much boiler plate code for a ViewModel Factory.
 *
 * Provides the SleepDatabaseDao and context to the ViewModel.
 */
class EventEditViewModelFactory(
    private val dataSource: EventDatabaseDao,
    private val storageDir: File?,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EventEditViewModel::class.java)) {
            return EventEditViewModel(dataSource, storageDir, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

