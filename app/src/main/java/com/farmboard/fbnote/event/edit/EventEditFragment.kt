package com.farmboard.fbnote.event.edit

import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.farmboard.fbnote.PictureTools
import com.farmboard.fbnote.R
import com.farmboard.fbnote.ViewHelperTools
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.database.MainDatabase
import com.farmboard.fbnote.databinding.FragmentEventEditBinding
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat

class EventEditFragment : Fragment() {
    lateinit var eventView: EventEditViewModel
    lateinit var binding: FragmentEventEditBinding
    lateinit var storageDir: File

    private var currentPhotoUri: Uri? = null

    private val getPicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
            currentPhotoUri?.let { uri ->
                val fullFile = fullPicturePath(uri)
                if (success) {
                    binding.imgPath = Event.imgPathFromUri(uri)
                    PictureTools.setPic(binding.eventEditThumbnail, fullFile.path)
                } else {
                    fullFile.delete()
                    currentPhotoUri = null
                }
            }
        }

    private fun fullPicturePath(uri: Uri): File {
        var imgFile = File("")
        for (pth in uri.pathSegments.subList(1, uri.pathSegments.size)) {
            imgFile = File(imgFile, pth)
        }
        return File(storageDir, imgFile.path)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!

        // create layout
        val placeId = EventEditFragmentArgs.fromBundle(requireArguments()).placeId
        if (placeId == "") {
            Timber.d("WAWA place not set")
            throw NotImplementedError()
        }
        binding = FragmentEventEditBinding.inflate(inflater, container, false)

        val items = listOf("msg", "pic", "planting", "death")
        val adapter = ArrayAdapter(requireContext(), R.layout.event_header_item, items)
        (binding.eventEditHeader.editText as? AutoCompleteTextView)?.setAdapter(adapter)
        binding.eventEditHeader.editText?.doOnTextChanged { text, _, _, _ ->
            if (text.toString() == "planting") {
                binding.eventEditText.editText?.let {
                    if (it.text.toString() == "") {
                        it.setText(getString(R.string.event_edit_tpl_planting))
                    }
                }
            }
        }

        binding.eventEditToolbar.inflateMenu(R.menu.event_edit_more)

        // create view model associated with fragment
        val application = requireNotNull(this.activity).application
        val dataSource = MainDatabase.getInstance(application).eventDatabaseDao


        val viewModelFactory = EventEditViewModelFactory(dataSource, storageDir, application)
        eventView = ViewModelProvider(
            this, viewModelFactory
        ).get(EventEditViewModel::class.java)

        val eventId = EventEditFragmentArgs.fromBundle(requireArguments()).eventId
        if (eventId == "") {
            eventView.create(placeId)
        } else {
            eventView.set(eventId)
        }

        // attach listener to events
        eventView.event.observe(viewLifecycleOwner, Observer { bindView(it) })

        binding.eventEditPicture.setOnClickListener {
            createPhotoUri()?.let {
                currentPhotoUri = it
                getPicture.launch(it)
            }
        }

        binding.eventEditToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.event_edit_more_delete -> {
                    delete()
                    true
                }
                R.id.event_edit_more_validate -> {
                    validate()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }

        return binding.root
    }

    private fun bindView(event: Event?) {
        event?.let {
            binding.eventEditToolbar.title = "Edit event"
            binding.eventEditToolbar.subtitle = it.eventId
            (binding.eventEditHeader.editText as? AutoCompleteTextView)?.setText(it.header, false)

            binding.eventEditDate.editText?.setText(SimpleDateFormat("yyyy-MM-dd HH:mm").format(it.date))
            binding.eventEditDate.isEnabled = false
            binding.imgPath = it.imgPath

            it.fullImgPath(storageDir)?.let { file ->
                PictureTools.setPic(binding.eventEditThumbnail, file.path)
                binding.eventEditPicture.isEnabled = false
            }

            binding.eventEditText.editText?.setText(it.text)
        }

    }

    private fun validate() {
        eventView.event.value?.let { event ->
            binding.eventEditHeader.editText?.let {
                eventView.setHeader(it.text.toString())
            }
            binding.imgPath?.let {
                eventView.setImgPath(it)
            }
            binding.eventEditText.editText?.let {
                eventView.setText(it.text.toString())
            }
            if (event.header == "") {
                if (event.imgPath != "") {
                    event.header = "pic"
                } else {
                    if (event.text != "") {
                        event.header = "msg"
                    } else {
                        Toast.makeText(
                            context,
                            "Header '${event.header}' undefined",
                            Toast.LENGTH_LONG
                        ).show()
                        return
                    }
                }
            }

            eventView.validate {
                ViewHelperTools.hideKeyboard(this)

                val action =
                    EventEditFragmentDirections.actionEventEditFragmentToEventDisplayFragment()
                action.eventId = event.eventId
                findNavController().navigate(action)
            }
        }
    }

    private fun delete() {
        eventView.event.value?.let {
            eventView.delete {
                ViewHelperTools.hideKeyboard(this)
                val action =
                    EventEditFragmentDirections.actionEventEditFragmentToPlaceDisplayFragment()
                action.placeId = it.place
                findNavController().navigate(action)
            }
        }
    }

    private fun createPhotoUri(): Uri? {
        eventView.event.value?.let { event ->
            val photoFile = event.fullImgPath(storageDir, Event.createImgPath())
            return FileProvider.getUriForFile(
                requireContext(),
                "com.farmboard.fileprovider",
                photoFile
            )
        }

        return null
    }

}