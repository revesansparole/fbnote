package com.farmboard.fbnote.event.display

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.database.EventDatabaseDao

class EventDisplayViewModelFactory(
    private val eventDB: EventDatabaseDao,
    private val eventId: String,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EventDisplayViewModel::class.java)) {
            return EventDisplayViewModel(eventDB, eventId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}