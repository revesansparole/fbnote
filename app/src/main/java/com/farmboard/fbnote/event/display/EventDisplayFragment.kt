package com.farmboard.fbnote.event.display

import android.os.Bundle
import android.os.Environment
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.farmboard.fbnote.PictureTools
import com.farmboard.fbnote.R
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.database.MainDatabase
import com.farmboard.fbnote.databinding.FragmentEventDisplayBinding
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class EventDisplayFragment : Fragment() {
    lateinit var binding: FragmentEventDisplayBinding
    lateinit var eventView: EventDisplayViewModel
    lateinit var storageDir: File

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        // create layout
        binding = FragmentEventDisplayBinding.inflate(inflater, container, false)
        binding.eventDisplayInfo.movementMethod = ScrollingMovementMethod()

        binding.eventDisplayToolbar.inflateMenu(R.menu.event_display_more)

        // create view model associated to this fragment
        val application = requireNotNull(this.activity).application
        val mainDB = MainDatabase.getInstance(application)

        val eventId = EventDisplayFragmentArgs.fromBundle(requireArguments()).eventId
        val viewModelFactory = EventDisplayViewModelFactory(
            mainDB.eventDatabaseDao,
            eventId,
            application
        )

        eventView = ViewModelProvider(
            this, viewModelFactory
        ).get(EventDisplayViewModel::class.java)

        // connect listeners
        binding.eventDisplayToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.event_display_more_edit -> {
                    edit()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }
        eventView.event.observe(viewLifecycleOwner, Observer { bindViewModel(it) })

        return binding.root
    }


    private fun bindViewModel(event: Event?) {
        event?.let {
            if (it.header == "") {
                binding.eventDisplayToolbar.title = "Event [${it.eventId}]"
            } else {
                binding.eventDisplayToolbar.title = it.header
            }
            binding.eventDisplayToolbar.subtitle =
                SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ROOT).format(it.date)

            if (it.text == "") {
                binding.eventDisplayInfo.isVisible = false
            } else {
                binding.eventDisplayInfo.text = it.text
            }

            val imgFile = it.fullImgPath(storageDir)
            if (imgFile == null) {
                binding.eventDisplayImg.isVisible = false
            } else {
                binding.eventDisplayImg.isVisible = true
                PictureTools.setPic(binding.eventDisplayImg, imgFile.path)
            }
        }

    }

    private fun edit() {
        eventView.event.value?.let {
            val action =
                EventDisplayFragmentDirections.actionEventDisplayFragmentToEventEditFragment()
            action.eventId = it.eventId
            action.placeId = it.place
            findNavController().navigate(action)
        }
    }

}