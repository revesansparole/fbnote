package com.farmboard.fbnote.event.edit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.database.EventDatabaseDao
import kotlinx.coroutines.launch
import java.io.File

class EventEditViewModel(
    val database: EventDatabaseDao,
    val storageDir: File?,
    application: Application
) : AndroidViewModel(application) {

    private val _event = MutableLiveData<Event?>()

    val event: LiveData<Event?>
        get() = _event

    fun set(eventId: String) {
        viewModelScope.launch {
            _event.value = database.get(eventId)
        }
    }

    fun create(place: String) {
        viewModelScope.launch {
            val event = Event.create(place)
            database.insert(event)
            _event.value = database.get(event.eventId)
        }
    }

    fun setHeader(header: String) {
        _event.value?.let {
            it.header = header
        }
    }

    fun setText(text: String) {
        _event.value?.let {
            it.text = text
        }
    }

    fun setImgPath(pth: String) {
        _event.value?.let {
            it.imgPath = pth
        }
    }

    fun validate(onFinished: () -> Unit) {
        viewModelScope.launch {
            _event.value?.let {
                database.update(it)
            }
            onFinished()
        }
    }

    fun delete(onFinished: () -> Unit) {
        viewModelScope.launch {
            _event.value?.let {
                database.delete(it, storageDir)
                _event.value = null
            }
            onFinished()
        }
    }

}

