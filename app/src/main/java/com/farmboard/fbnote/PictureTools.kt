package com.farmboard.fbnote

import android.graphics.BitmapFactory
import android.widget.ImageView

class PictureTools {
    companion object {
        fun setPic(imageView: ImageView, path: String) {
            // Get the dimensions of the View
            val targetW: Int = Math.max(60,imageView.width)  // TODO
            val targetH: Int = Math.max(60,imageView.height)

            val bmOptions = BitmapFactory.Options().apply {
                // Get the dimensions of the bitmap
                inJustDecodeBounds = true

                BitmapFactory.decodeFile(path, this)

                val photoW: Int = outWidth
                val photoH: Int = outHeight

                // Determine how much to scale down the image
                val scaleFactor: Int = Math.max(1, Math.min(photoW / targetW, photoH / targetH))
                // Decode the image file into a Bitmap sized to fill the View
                inJustDecodeBounds = false
                inSampleSize = scaleFactor
            }
            BitmapFactory.decodeFile(path, bmOptions)?.also { bitmap ->
                imageView.setImageBitmap(bitmap)
            }
        }

    }
}