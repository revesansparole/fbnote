package com.farmboard.fbnote.database.manager

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.database.EventDatabaseDao
import com.farmboard.fbnote.database.PlaceDatabaseDao
import java.io.File

/**
 * This is pretty much boiler plate code for a ViewModel Factory.
 *
 * Provides the SleepDatabaseDao and context to the ViewModel.
 */
class DbManagerViewModelFactory(
    private val placeDB: PlaceDatabaseDao,
    private val eventDB: EventDatabaseDao,
    private val storageDir: File?,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DbManagerViewModel::class.java)) {
            return DbManagerViewModel(placeDB, eventDB, storageDir, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

