package com.farmboard.fbnote.database.manager

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.MainActivity
import com.farmboard.fbnote.R
import com.farmboard.fbnote.database.MainDatabase
import com.farmboard.fbnote.databinding.FragmentDbManagerBinding
import timber.log.Timber
import java.io.File

class DbManagerFragment : Fragment() {
    private val exchangeDir = File("/storage/emulated/0/farmboard/")
    private lateinit var storageDir: File
    private lateinit var dbManager: DbManagerViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        // inflate layout
        val binding = FragmentDbManagerBinding.inflate(inflater, container, false)
        binding.dbManagerToolbar.inflateMenu(R.menu.db_manager_more)

        // associate view model to this fragment
        val application = requireNotNull(this.activity).application
        val mainDB = MainDatabase.getInstance(application)

        val viewModelFactory =
            DbManagerViewModelFactory(
                mainDB.placeDatabaseDao,
                mainDB.eventDatabaseDao,
                storageDir,
                application
            )
        dbManager = ViewModelProvider(
            this, viewModelFactory
        ).get(DbManagerViewModel::class.java)

        // connect listeners
        dbManager.info.observe(viewLifecycleOwner, Observer { binding.dbManagerInfo.text = it })
        dbManager.refreshInfo()

        binding.dbManagerToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.db_manager_more_export -> {
                    Timber.d("WAWA export")
                    exportDb()
                    true
                }
                R.id.db_manager_more_import -> {
                    Timber.d("WAWA import")
                    importDb()
                    true
                }
                R.id.db_manager_more_clean -> {
                    Timber.d("WAWA clean")
                    cleanDb()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }

        // check permissions to write on external storage
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                activity as MainActivity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1
            )
            Timber.d("WAWA require permission")
        } else {
            Timber.d("WAWA all the good permissions")
        }

        return binding.root
    }

    private fun exportDb() {
        // check export dir exists and is empty
        if (!exchangeDir.exists()) {
            exchangeDir.mkdirs()
        } else {
            exchangeDir.list()?.let {
                if (it.isNotEmpty()) {
                    Toast.makeText(this.context, "$exchangeDir not empty", Toast.LENGTH_LONG).show()
                    return
                }
            }
        }

        // export
        dbManager.export(storageDir, exchangeDir) {
            Toast.makeText(this.context, "export finished in $exchangeDir", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun importDb() {
        // check export dir exists and is not empty
        if (!exchangeDir.exists()) {
            Toast.makeText(this.context, "$exchangeDir does not exists", Toast.LENGTH_LONG).show()
            return
        }
        val files = exchangeDir.list()
        if (files == null) {
            Toast.makeText(this.context, "$exchangeDir is empty", Toast.LENGTH_LONG).show()
            return
        }
        if (files.isEmpty()) {
            Toast.makeText(this.context, "$exchangeDir is empty", Toast.LENGTH_LONG).show()
            return
        }

        // import
        dbManager.import(storageDir, exchangeDir) {
            Toast.makeText(this.context, "import finished from $exchangeDir", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun cleanDb() {
        // remove everything from database
        dbManager.clean(storageDir) {
            Toast.makeText(this.context, "db empty", Toast.LENGTH_LONG)
                .show()
        }
    }

}