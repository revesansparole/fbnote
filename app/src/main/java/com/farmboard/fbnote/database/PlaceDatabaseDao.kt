package com.farmboard.fbnote.database

import androidx.lifecycle.LiveData
import androidx.room.*
import java.io.File

@Dao
interface PlaceDatabaseDao {
    @Insert
    suspend fun insertDb(place: Place)

    suspend fun insert(place: Place, storageDir: File) {
        insertDb(place)
        val placeDir = place.dataDir(storageDir)
        if (!placeDir.exists()) {
            placeDir.mkdir()
        }
    }

    @Update
    suspend fun update(place: Place)

    @Delete
    suspend fun delete(place: Place)

    @Query("SELECT * from place_table WHERE placeId = :key")
    suspend fun get(key: String): Place?

    @Query("DELETE FROM place_table")
    suspend fun clear()

    @Query("SELECT * from place_table WHERE placeId = :key")
    fun getSingle(key: String): LiveData<Place?>

    @Query("SELECT * FROM place_table ORDER BY placeId DESC")
    fun getAllPlaces(): LiveData<List<Place>>

    @Query("SELECT * FROM place_table")
    fun getDebugAllPlaces(): LiveData<List<Place>>

    @Query("SELECT * FROM place_table")
    suspend fun getManageAllPlaces(): List<Place>

    @Query("SELECT * FROM place_table ORDER BY ABS(latitude - :latitude) + ABS(longitude - :longitude) ASC")
    fun findByDistance(latitude: Double, longitude: Double): LiveData<List<Place>>

    suspend fun changeId(place: Place, newPlaceId: String, storageDir: File){
        val placeDir = place.dataDir(storageDir)

        // delete place
        delete(place)

        // insert place with new id
        place.placeId = newPlaceId
        insertDb(place)

        // update associated data dir
        val newPlaceDir = place.dataDir(storageDir)
        if (!placeDir.exists()) {
            throw Exception("data dir does not exists")
        }
        if (newPlaceDir.exists()){
            throw Exception("dst dir already exists")
        }
        if (!placeDir.renameTo(newPlaceDir)) {
            throw Exception("unable to rename dir $newPlaceDir")
        }
    }
}