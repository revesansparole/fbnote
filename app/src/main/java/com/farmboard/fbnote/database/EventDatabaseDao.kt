package com.farmboard.fbnote.database

import androidx.lifecycle.LiveData
import androidx.room.*
import java.io.File

@Dao
interface EventDatabaseDao {
    @Insert
    suspend fun insert(event: Event)

    @Update
    suspend fun update(event: Event)

    @Delete
    suspend fun deleteDb(event: Event)

    @Query("SELECT * from event_table WHERE eventId = :key")
    suspend fun get(key: String): Event?

    @Query("DELETE FROM event_table")
    suspend fun clear()

    @Query("SELECT * from event_table WHERE eventId = :key")
    fun getSingle(key: String): LiveData<Event?>

    @Query("SELECT * FROM event_table WHERE place = :place ORDER BY date DESC")
    fun getAllEvents(place: String): LiveData<List<Event>>

    @Query("SELECT * FROM event_table WHERE place = :place")
    suspend fun getPlaceEvents(place: String): List<Event>

    @Query("SELECT * FROM event_table")
    fun getDebugAllEvents(): LiveData<List<Event>>

    @Query("SELECT * FROM event_table")
    suspend fun getManageAllEvents(): List<Event>

    suspend fun delete(event: Event, storageDir: File?) {
        storageDir?.let{dir->
            event.fullImgPath(dir)?.delete()
        }
        deleteDb(event)
    }

}