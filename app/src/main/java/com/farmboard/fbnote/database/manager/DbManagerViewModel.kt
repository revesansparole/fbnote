package com.farmboard.fbnote.database.manager

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.database.EventDatabaseDao
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.database.PlaceDatabaseDao
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File

class DbManagerViewModel(
    private val placeDB: PlaceDatabaseDao,
    private val eventDB: EventDatabaseDao,
    val storageDir: File?,
    application: Application
) : AndroidViewModel(application) {

    private val _info = MutableLiveData<String>()

    val info: LiveData<String>
        get() = _info

    fun refreshInfo() {
        viewModelScope.launch {
            val places = placeDB.getManageAllPlaces()
            val events = eventDB.getManageAllEvents()
            _info.value = "%s \n %s".format(places.toString(), events.toString())
        }
    }

    fun clean(storageDir: File, onEnded: () -> Unit) {
        viewModelScope.launch {
            for (place in placeDB.getManageAllPlaces()) {
                // remove associated events
                for (event in eventDB.getPlaceEvents(place.placeId)) {
                    eventDB.delete(event, storageDir)
                }
                // remove associated dir for events data
                place.dataDir(storageDir).delete()
                // remove place from DB
                placeDB.delete(place)
            }
            // just to be on the safe side
            placeDB.clear()
            eventDB.clear()

            storageDir.listFiles()?.let {
                for (file in it) {
                    if (file.isDirectory) {
                        file.listFiles()?.let{eventFiles ->
                            for (eventFile in eventFiles) {
                                eventFile.delete()
                                Timber.d("WAWA delete $eventFile")
                            }
                        }
                    }

                    val res = file.delete()
                    Timber.d("WAWA delete $file, $res")
                }

            }
            // finished
            onEnded()
        }
    }

    fun export(storageDir: File, exchangeDir: File, onEnded: () -> Unit) {
        viewModelScope.launch {
            // export all places
            for (place in placeDB.getManageAllPlaces()) {
                Timber.d("WAWA export place $place")
                val placeDir = place.dataDir(exchangeDir)
                val placeInfo = File(placeDir, "info.json")

                placeDir.mkdir()
                placeInfo.createNewFile()
                placeInfo.writeText(place.toJson())
            }

            // export all events
            for (event in eventDB.getManageAllEvents()) {
                Timber.d("WAWA export event $event")
                val placeDir = Place.dataDir(exchangeDir, event.place)
                val eventInfo = File(placeDir, "event_%s_info.json".format(event.eventId))

                eventInfo.createNewFile()
                eventInfo.writeText(event.toJson())
                event.fullImgPath(storageDir)?.copyTo(event.fullImgPath(exchangeDir)!!)
            }

            // finished
            onEnded()
        }

    }

    fun import(storageDir: File, exchangeDir: File, onEnded: () -> Unit) {
        viewModelScope.launch {
            exchangeDir.listFiles { file -> file.isDirectory }?.let { placeDirList ->
                for (placeDir in placeDirList) {
                    // import place if needed
                    val placeInfo = File(placeDir, "info.json")
                    if (placeInfo.exists()) {
                        Timber.d("WAWA import place from $placeInfo")
                        val place = Place.fromJson(placeInfo.readText())
                        if (placeDB.get(place.placeId) == null) {
                            placeDB.insert(place, storageDir)
                        } else {
                            placeDB.update(place)
                        }
                    }

                    // import events
                    placeDir.listFiles { file -> file.name.startsWith("event_") }
                        ?.let { eventFileList ->
                            for (eventInfo in eventFileList) {
                                Timber.d("WAWA import event from $eventInfo")
                                val event = Event.fromJson(eventInfo.readText())
                                if (eventDB.get(event.eventId) != null) {
                                    eventDB.delete(event, storageDir)
                                }
                                eventDB.insert(event)
                                event.fullImgPath(exchangeDir)
                                    ?.copyTo(event.fullImgPath(storageDir)!!)
                            }
                        }
                }
            }

            // finished
            onEnded()
        }
    }
}