package com.farmboard.fbnote.database

import android.net.Uri
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "event_table")
data class Event(
    @NonNull
    @PrimaryKey
    var eventId: String,

    @NonNull
    @ColumnInfo(name = "place")
    var place: String,

    @ColumnInfo(name = "date")
    var date: Date = Date(),

    @ColumnInfo(name = "header")
    var header: String = "",

    @ColumnInfo(name = "txt")
    var text: String = "",

    @ColumnInfo(name = "img_pth")
    var imgPath: String = "",
) {
    fun fullImgPath(storageDir: File): File? {
        return if (imgPath == "") {
            null
        } else {
            fullImgPath(storageDir, imgPath)
        }
    }

    fun fullImgPath(storageDir: File, imgPath: String): File {
        val placeDir = Place.dataDir(storageDir, place)
        return File(placeDir, imgPath)
    }

    fun toJson(): String {
        val eventType = object : TypeToken<Event>() {}.type
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").setPrettyPrinting().create()
        return gson.toJson(this, eventType)
    }


    companion object {
        fun create(place: String): Event {
            return Event(eventId = UUID.randomUUID().toString(), place=place)
        }

        fun createImgPath(): String {
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT).format(Date())
            return "IMG_$timeStamp.jpg"
        }

        fun imgPathFromUri(uri: Uri): String {
            return uri.pathSegments[uri.pathSegments.size - 1]
        }

        fun fromJson(jsonRepr: String): Event {
            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
            return gson.fromJson(jsonRepr, Event::class.java)
        }
    }
}