package com.farmboard.fbnote.database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.util.*

@Entity(tableName = "place_table")
data class Place(
    @NonNull
    @PrimaryKey
    var placeId: String,

    @ColumnInfo(name = "name")
    var name: String = "",

    @ColumnInfo(name = "latitude")
    var latitude: Double = 0.0,

    @ColumnInfo(name = "longitude")
    var longitude: Double = 0.0,

    @ColumnInfo(name = "altitude")
    var altitude: Double = 0.0,
) {
    fun dataDir(storageDir: File): File {
        return dataDir(storageDir, placeId)
    }

    fun toJson(): String {
        val placeType = object : TypeToken<Place>() {}.type
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").setPrettyPrinting().create()
        return gson.toJson(this, placeType)
    }

    companion object {
        fun create(): Place {
            return Place(placeId = UUID.randomUUID().toString())

        }

        fun dataDir(storageDir: File, placeId: String): File {
            return File(storageDir, placeId)
        }

        fun fromJson(jsonRepr: String): Place {
            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
            return gson.fromJson(jsonRepr, Place::class.java)
        }
    }

}