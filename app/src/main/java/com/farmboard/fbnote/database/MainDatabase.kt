package com.farmboard.fbnote.database

import android.content.Context
import androidx.room.*
import java.util.*

@Database(entities = [Place::class, Event::class], version = 1, exportSchema = false)
@TypeConverters(DateConverters::class)
abstract class MainDatabase : RoomDatabase() {
    abstract val placeDatabaseDao: PlaceDatabaseDao
    abstract val eventDatabaseDao: EventDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: MainDatabase? = null

        fun getInstance(context: Context): MainDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MainDatabase::class.java,
                        "fbnote_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }


    }

}


class DateConverters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}
