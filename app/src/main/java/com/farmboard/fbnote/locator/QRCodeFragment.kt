package com.farmboard.fbnote.locator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import timber.log.Timber
import java.util.*


class QRCodeFragment : Fragment(), ZXingScannerView.ResultHandler {
    private lateinit var mScannerView: ZXingScannerView
    private lateinit var requester: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        requester = QRCodeFragmentArgs.fromBundle(requireArguments()).requester

        mScannerView = ZXingScannerView(this.context)
        return mScannerView

    }

    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

    override fun handleResult(rawResult: Result) {
        val extractedId = rawResult.text
        try {
            UUID.fromString(extractedId)
            Timber.d("WAWA qrcode, pid: $extractedId")
            when (requester) {
                "" -> {  // nobody hence display
                    Timber.d("WAWA display $extractedId")
                    val action =
                        QRCodeFragmentDirections.actionQRCodeFragmentToPlaceDisplayFragment()
                    action.placeId = extractedId
                    findNavController().navigate(action)
                }
                else -> {  //hopefully some valid placeId
                    Timber.d("WAWA edit $requester")
                    val action = QRCodeFragmentDirections.actionQRCodeFragmentToPlaceEditFragment()
                    action.placeId = requester
                    action.newPlaceId = extractedId
                    findNavController().navigate(action)
                }
            }
        } catch (exception: IllegalArgumentException) {
            Toast.makeText(context, "QRCode msg '$extractedId' not recognized", Toast.LENGTH_LONG)
                .show()
            mScannerView.resumeCameraPreview(this)
        }
    }
}