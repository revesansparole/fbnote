package com.farmboard.fbnote

import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

class ViewHelperTools {
    companion object {
        fun hideKeyboard(fragment: Fragment) {
            fragment.context?.let { context ->
                val inputMethodManager =
                    ContextCompat.getSystemService(context, InputMethodManager::class.java)!!
                fragment.view?.let { view ->
                    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
        }
    }
}