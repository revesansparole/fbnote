package com.farmboard.fbnote.place.display

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.farmboard.fbnote.PictureTools
import com.farmboard.fbnote.R
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.databinding.ListItemEventBinding
import java.io.File
import java.text.SimpleDateFormat

class EventListAdapter(
    private val storageDir: File,
    private val clickListener: EventListItemListener
) :
    ListAdapter<Event, EventListAdapter.ViewHolder>(EventDiffCallback()) {

    class ViewHolder private constructor(val binding: ListItemEventBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemEventBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

        fun bind(item: Event, clickListener: EventListItemListener, storageDir: File) {
            binding.event = item
            binding.clickListener = clickListener
            // fill display attributes
            if (item.imgPath != "") {
                item.fullImgPath(storageDir)?.let {
                    PictureTools.setPic(binding.eventItemThumbnail, it.path)
                }
            } else {
                when (item.header) {
                    "msg" -> binding.eventItemThumbnail.setImageResource(R.drawable.ic_baseline_sms_24)
                    "planting" -> binding.eventItemThumbnail.setImageResource(R.drawable.ic_baseline_grass_24)
                    else -> binding.eventItemThumbnail.setImageResource(R.drawable.ic_baseline_alarm_on_24)
                }
            }
            val dateString = SimpleDateFormat("yyyy-MM-dd").format(item.date)
            binding.eventItemHeader.text = "%s %s".format(dateString, item.header)
            val descr = if (item.text.length < 30) {
                item.text
            } else {
                "%s...".format(item.text.subSequence(0, 30))
            }
            binding.eventItemDescr.text = descr
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener, storageDir)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

}


class EventDiffCallback : DiffUtil.ItemCallback<Event>() {

    override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem.eventId == newItem.eventId
    }

    override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem == newItem
    }


}

class EventListItemListener(val clickListener: (eventId: String) -> Unit) {
    fun onClick(event: Event) = clickListener(event.eventId)
}
