package com.farmboard.fbnote.place.manager

import android.app.Application
import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.database.PlaceDatabaseDao

/**
 * This is pretty much boiler plate code for a ViewModel Factory.
 *
 * Provides the SleepDatabaseDao and context to the ViewModel.
 */
class PlaceManagerViewModelFactory(
    val loc: Location,
    private val dataSource: PlaceDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlaceManagerViewModel::class.java)) {
            return PlaceManagerViewModel(loc, dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

