package com.farmboard.fbnote.place.manager

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.database.PlaceDatabaseDao

class PlaceManagerViewModel(
    loc: Location,
    val database: PlaceDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    private var _places = database.findByDistance(loc.latitude, loc.longitude)

    val places: LiveData<List<Place>>
        get() = _places

    fun updatePosition(loc: Location) {
        _places = database.findByDistance(loc.latitude, loc.longitude)
    }
}