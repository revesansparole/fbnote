package com.farmboard.fbnote.place.edit

import android.content.DialogInterface
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.farmboard.fbnote.R
import com.farmboard.fbnote.ViewHelperTools
import com.farmboard.fbnote.database.MainDatabase
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.databinding.FragmentPlaceEditBinding
import kotlinx.coroutines.launch
import timber.log.Timber

class PlaceEditFragment : Fragment() {
    lateinit var placeView: PlaceEditViewModel
    lateinit var binding: FragmentPlaceEditBinding
    lateinit var placeId: String
    lateinit var mainDB: MainDatabase

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!

        // create layout
        binding = FragmentPlaceEditBinding.inflate(inflater, container, false)

        binding.placeEditToolbar.inflateMenu(R.menu.place_edit_more)

        // create model view associated to this fragment
        val application = requireNotNull(this.activity).application
        mainDB = MainDatabase.getInstance(application)

        val viewModelFactory = PlaceEditViewModelFactory(
            mainDB.placeDatabaseDao,
            mainDB.eventDatabaseDao,
            storageDir,
            application
        )
        placeView = ViewModelProvider(
            this, viewModelFactory
        ).get(PlaceEditViewModel::class.java)

        placeId = PlaceEditFragmentArgs.fromBundle(requireArguments()).placeId
        if (placeId == "") {
            val loc = PlaceEditFragmentArgs.fromBundle(requireArguments()).loc
            if (loc == null) {
                Toast.makeText(context, "Location undefined", Toast.LENGTH_LONG).show()
                val action =
                    PlaceEditFragmentDirections.actionPlaceEditFragmentToPlaceManagerFragment()
                findNavController().navigate(action)
            } else {
                placeView.create(loc)
            }
        } else {
            placeView.set(placeId)
        }

        val newPlaceId = PlaceEditFragmentArgs.fromBundle(requireArguments()).newPlaceId
        if (newPlaceId != "") {
            changeId(newPlaceId)
        }

        // connect listeners
        placeView.place.observe(viewLifecycleOwner, Observer {
            placeId = it.placeId
            bindView(it)
        })

        binding.placeEditToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.place_edit_more_delete -> {
                    delete()
                    true
                }
                R.id.place_edit_more_validate -> {
                    validate()
                    true
                }
                R.id.place_edit_more_qr -> {
                    val action =
                        PlaceEditFragmentDirections.actionPlaceEditFragmentToQRCodeFragment()
                    action.requester = placeId
                    findNavController().navigate(action)
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }

        return binding.root
    }

    private fun bindView(place: Place?) {
        place?.let {
            binding.placeEditToolbar.title = "Edit place"
            binding.placeEditToolbar.subtitle = it.placeId
            binding.placeEditName.editText?.setText(it.name)
            binding.placeEditLatitude.editText?.setText(it.latitude.toString())
            binding.placeEditLongitude.editText?.setText(it.longitude.toString())
            binding.placeEditAltitude.editText?.setText(it.altitude.toString())
        }
    }

    private fun isValidInputs(): Boolean {
        // check validity of name
        val name = binding.placeEditName.editText?.text.toString()
        if (name == "") {
            Toast.makeText(context, "Name '$name' undefined", Toast.LENGTH_LONG).show()
            return false
        }

        // check validity of latitude
        val latitudeStr = binding.placeEditLatitude.editText?.text.toString()
        try {
            val latitude = latitudeStr.toDouble()
            if (latitude < -90.0 || latitude > 90.0) {
                Toast.makeText(context, "Latitude '$latitude' not correct", Toast.LENGTH_LONG)
                    .show()
                return false
            }
        } catch (e: NumberFormatException) {
            Toast.makeText(context, "Latitude '$latitudeStr' not correct", Toast.LENGTH_LONG)
                .show()
            return false
        }

        // check validity of longitude
        val longitudeStr = binding.placeEditLongitude.editText?.text.toString()
        try {
            val longitude = longitudeStr.toDouble()
            if (longitude < -90.0 || longitude > 90.0) {
                Toast.makeText(context, "Longitude '$longitude' not correct", Toast.LENGTH_LONG)
                    .show()
                return false
            }
        } catch (e: NumberFormatException) {
            Toast.makeText(context, "Longitude '$longitudeStr' not correct", Toast.LENGTH_LONG)
                .show()
            return false
        }

        // check validity of altitude
        val altitudeStr = binding.placeEditAltitude.editText?.text.toString()
        try {
            altitudeStr.toDouble()
        } catch (e: NumberFormatException) {
            Toast.makeText(context, "Altitude '$altitudeStr' not correct", Toast.LENGTH_LONG)
                .show()
            return false
        }

        return true
    }

    private fun validate() {
        placeView.place.value?.let {
            if (isValidInputs()) {
                placeView.setName(binding.placeEditName.editText?.text.toString())
                placeView.setLatitude(
                    binding.placeEditLatitude.editText?.text.toString().toDouble()
                )
                placeView.setLongitude(
                    binding.placeEditLongitude.editText?.text.toString().toDouble()
                )
                placeView.setAltitude(
                    binding.placeEditAltitude.editText?.text.toString().toDouble()
                )

                placeView.validate {
                    ViewHelperTools.hideKeyboard(this)
                    val action =
                        PlaceEditFragmentDirections.actionPlaceEditFragmentToPlaceDisplayFragment()
                    action.placeId = it.placeId
                    findNavController().navigate(action)
                }
            }
        }
    }

    private fun delete() {
        placeView.delete {
            ViewHelperTools.hideKeyboard(this)
            val action =
                PlaceEditFragmentDirections.actionPlaceEditFragmentToPlaceManagerFragment()
            findNavController().navigate(action)
        }
    }

    private fun changeId(newPlaceId: String) {
        lifecycleScope.launch {
            if (mainDB.placeDatabaseDao.get(newPlaceId) == null) {
                Timber.d("WAWA request to change id")
                val alertDialog: AlertDialog? = activity?.let {
                    val builder = AlertDialog.Builder(it)
                    builder.setMessage("Change id?")
                        .setTitle("New id: %s".format(newPlaceId))
                    builder.apply {
                        setPositiveButton("Ok",
                            DialogInterface.OnClickListener { dialog, id ->
                                Timber.d("WAWA OK")
                                placeView.changeId(newPlaceId)
                            })
                        setNegativeButton("Cancel",
                            DialogInterface.OnClickListener { dialog, id ->
                                Timber.d("WAWA cancel new id $dialog, $id")
                            })
                    }
                    builder.create()
                }

                alertDialog?.show()

            } else {
                Toast.makeText(
                    context,
                    "Cancelled id $newPlaceId already exists",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

}