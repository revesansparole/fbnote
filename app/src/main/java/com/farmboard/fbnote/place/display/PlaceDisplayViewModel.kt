package com.farmboard.fbnote.place.display

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.farmboard.fbnote.database.Event
import com.farmboard.fbnote.database.EventDatabaseDao
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.database.PlaceDatabaseDao

class PlaceDisplayViewModel(
    placeDB: PlaceDatabaseDao,
    eventDB: EventDatabaseDao,
    placeId: String,
    application: Application
) : AndroidViewModel(application) {

    private val _place = placeDB.getSingle(placeId)

    val place: LiveData<Place?>
        get() = _place

    private val _events = eventDB.getAllEvents(placeId)

    val events: LiveData<List<Event>>
        get() = _events

}