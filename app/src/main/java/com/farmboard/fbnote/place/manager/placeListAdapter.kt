package com.farmboard.fbnote.place.manager

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.databinding.ListItemPlaceBinding

class PlaceListAdapter(val clickListener: PlaceListItemListener) :
    ListAdapter<Place, PlaceListAdapter.ViewHolder>(PlaceDiffCallback()) {

    class ViewHolder private constructor(val binding: ListItemPlaceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemPlaceBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

        fun bind(item: Place, clickListener: PlaceListItemListener) {
            binding.place = item
            binding.clickListener = clickListener
            // fill display attributes
            binding.placeItemDescr.text = item.name

        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

}


class PlaceDiffCallback : DiffUtil.ItemCallback<Place>() {

    override fun areItemsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem.placeId == newItem.placeId
    }

    override fun areContentsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem == newItem
    }


}

class PlaceListItemListener(val clickListener: (placeId: String) -> Unit) {
    fun onClick(place: Place) = clickListener(place.placeId)
}
