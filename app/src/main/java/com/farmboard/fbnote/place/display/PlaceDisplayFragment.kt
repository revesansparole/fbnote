package com.farmboard.fbnote.place.display

import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.farmboard.fbnote.R
import com.farmboard.fbnote.database.MainDatabase
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.databinding.FragmentPlaceDisplayBinding
import timber.log.Timber
import java.util.*

class PlaceDisplayFragment : Fragment() {
    lateinit var binding: FragmentPlaceDisplayBinding
    lateinit var placeView: PlaceDisplayViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate overall layout
        binding = FragmentPlaceDisplayBinding.inflate(inflater, container, false)

        binding.placeDisplayToolbar.inflateMenu(R.menu.place_display_more)

        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val adapter = EventListAdapter(storageDir, EventListItemListener { eventId ->
            Timber.d("WAWA display event $eventId")
            val action =
                PlaceDisplayFragmentDirections.actionPlaceDisplayFragmentToEventDisplayFragment()
            action.eventId = eventId
            findNavController().navigate(action)
        })

        binding.placeDisplayTimeline.adapter = adapter
        binding.placeDisplayTimeline.layoutManager = LinearLayoutManager(activity)

        // create view model associated to this fragment
        val application = requireNotNull(this.activity).application
        val mainDB = MainDatabase.getInstance(application)

        val placeId = PlaceDisplayFragmentArgs.fromBundle(requireArguments()).placeId
        val viewModelFactory = PlaceDisplayViewModelFactory(
            mainDB.placeDatabaseDao,
            mainDB.eventDatabaseDao,
            placeId,
            application
        )
        placeView = ViewModelProvider(
            this, viewModelFactory
        ).get(PlaceDisplayViewModel::class.java)

        // connect listeners
        placeView.place.observe(viewLifecycleOwner, Observer { bindViewModel(it) })

        placeView.events.observe(viewLifecycleOwner, Observer { adapter.submitList(it) })

        binding.placeDisplayToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.place_display_more_edit -> {
                    edit()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }

        binding.fabAddEvent.setOnClickListener { createEvent() }

        return binding.root
    }

    private fun bindViewModel(place: Place?) {
        place?.let {
            if (it.name == "") {
                binding.placeDisplayToolbar.title = "Place [${it.placeId}]"
            } else {
                binding.placeDisplayToolbar.title = it.name
            }
            binding.placeDisplayToolbar.subtitle =
                "lat: %.2f, lon: %.2f".format(Locale.ENGLISH, it.latitude, it.longitude)
        }
    }

    private fun createEvent() {
        placeView.place.value?.let {
            val action =
                PlaceDisplayFragmentDirections.actionPlaceDisplayFragmentToEventEditFragment()
            action.placeId = it.placeId
            action.eventId = ""
            findNavController().navigate(action)
        }
    }

    private fun edit() {
        placeView.place.value?.let {
            val action =
                PlaceDisplayFragmentDirections.actionPlaceDisplayFragmentToPlaceEditFragment()
            action.placeId = it.placeId
            findNavController().navigate(action)
        }
    }
}