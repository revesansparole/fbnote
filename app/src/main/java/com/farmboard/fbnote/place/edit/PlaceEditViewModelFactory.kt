package com.farmboard.fbnote.place.edit

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.database.EventDatabaseDao
import com.farmboard.fbnote.database.PlaceDatabaseDao
import java.io.File

/**
 * This is pretty much boiler plate code for a ViewModel Factory.
 *
 * Provides the SleepDatabaseDao and context to the ViewModel.
 */
class PlaceEditViewModelFactory(
    private val placeDB: PlaceDatabaseDao,
    private val eventDB: EventDatabaseDao,
    private val storageDir: File,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlaceEditViewModel::class.java)) {
            return PlaceEditViewModel(placeDB, eventDB, storageDir, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

