package com.farmboard.fbnote.place.display


import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.farmboard.fbnote.database.EventDatabaseDao
import com.farmboard.fbnote.database.PlaceDatabaseDao

/**
 * This is pretty much boiler plate code for a ViewModel Factory.
 *
 * Provides the SleepDatabaseDao and context to the ViewModel.
 */
class PlaceDisplayViewModelFactory(
    private val placeDB: PlaceDatabaseDao,
    private val eventDB: EventDatabaseDao,
    private val placeId: String,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlaceDisplayViewModel::class.java)) {
            return PlaceDisplayViewModel(placeDB, eventDB, placeId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

