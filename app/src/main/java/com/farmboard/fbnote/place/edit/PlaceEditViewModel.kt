package com.farmboard.fbnote.place.edit

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.farmboard.fbnote.database.EventDatabaseDao
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.database.PlaceDatabaseDao
import kotlinx.coroutines.launch
import java.io.File

class PlaceEditViewModel(
    private val placeDB: PlaceDatabaseDao,
    private val eventDB: EventDatabaseDao,
    val storageDir: File,
    application: Application
) : AndroidViewModel(application) {

    private val _place = MutableLiveData<Place>()

    val place: LiveData<Place>
        get() = _place

    fun set(placeId: String) {
        viewModelScope.launch {
            _place.value = placeDB.get(placeId)
        }
    }

    fun changeId(newPlaceId: String) {
        _place.value?.let { place ->
            viewModelScope.launch {
                // change place in associated events
                for (event in eventDB.getPlaceEvents(place.placeId)) {
                    event.place = newPlaceId
                    eventDB.update(event)
                }
                // update place
                placeDB.changeId(place, newPlaceId, storageDir)
                // relaunch place
                _place.value = placeDB.get(place.placeId)
            }
        }
    }

    fun create(loc: Location) {
        viewModelScope.launch {
            val place = Place.create()
            place.latitude = loc.latitude
            place.longitude = loc.longitude
            place.altitude = loc.altitude
            placeDB.insert(place, storageDir)
            // relaunch place
            _place.value = placeDB.get(place.placeId)
        }
    }

    fun setName(name: String) {
        _place.value?.let {
            it.name = name
        }
    }

    fun setLatitude(latitude: Double) {
        _place.value?.let {
            it.latitude = latitude
        }
    }

    fun setLongitude(longitude: Double) {
        _place.value?.let {
            it.longitude = longitude
        }
    }

    fun setAltitude(altitude: Double) {
        _place.value?.let {
            it.altitude = altitude
        }
    }

    fun validate(onFinished: () -> Unit) {
        viewModelScope.launch {
            _place.value?.let {
                placeDB.update(it)
            }
            onFinished()
        }
    }

    fun delete(onFinished: () -> Unit) {
        viewModelScope.launch {
            _place.value?.let { place ->
                // remove associated events
                for (event in eventDB.getPlaceEvents(place.placeId)) {
                    eventDB.delete(event, storageDir)
                }
                // remove associated dir for events data
                place.dataDir(storageDir).delete()
                // remove place from DB
                placeDB.delete(place)
            }
            onFinished()
        }
    }

}

