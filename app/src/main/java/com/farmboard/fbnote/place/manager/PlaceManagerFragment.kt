package com.farmboard.fbnote.place.manager

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.farmboard.fbnote.R
import com.farmboard.fbnote.database.MainDatabase
import com.farmboard.fbnote.database.Place
import com.farmboard.fbnote.databinding.FragmentPlaceManagerBinding
import timber.log.Timber

class PlaceManagerFragment : Fragment(), LocationListener {
    lateinit var binding: FragmentPlaceManagerBinding
    lateinit var mainDB: MainDatabase

    var latestLocation: Location = Location(LocationManager.GPS_PROVIDER)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // inflate layout
        binding = FragmentPlaceManagerBinding.inflate(inflater, container, false)

        binding.placeManagerToolbar.inflateMenu(R.menu.place_manager_more)

        // start to acquire GPS position
        initLocationService()

        // associate view model to this fragment
        val application = requireNotNull(this.activity).application
        mainDB = MainDatabase.getInstance(application)

        val viewModelFactory =
            PlaceManagerViewModelFactory(
                latestLocation,
                mainDB.placeDatabaseDao,
                application
            )
        val placeManager = ViewModelProvider(
            this, viewModelFactory
        ).get(PlaceManagerViewModel::class.java)


        val adapter = PlaceListAdapter(PlaceListItemListener { placeId ->
            val action =
                PlaceManagerFragmentDirections.actionPlaceManagerFragmentToPlaceDisplayFragment()
            action.placeId = placeId
            findNavController().navigate(action)
        })

        binding.placesListView.layoutManager = LinearLayoutManager(activity)
        binding.placesListView.adapter = adapter

        // connect listeners
        val observer by lazy {
            Observer<List<Place>> { adapter.submitList(it) }
        }
        placeManager.places.observe(viewLifecycleOwner, observer)

        binding.fabAddPlace.setOnClickListener { createPlace() }

        binding.placeManagerToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.place_manager_more_refresh_pos -> {
                    Timber.d("WAWA refresh")
                    placeManager.places.removeObserver(observer)
                    placeManager.updatePosition(latestLocation)
                    placeManager.places.observe(viewLifecycleOwner, observer)
                    item.isEnabled = false
                    true
                }
                R.id.place_manager_more_qr -> {
                    val action =
                        PlaceManagerFragmentDirections.actionPlaceManagerFragmentToQRCodeFragment()
                    findNavController().navigate(action)
                    true
                }
                R.id.place_manager_more_info -> {
                    info()
                    true
                }
                R.id.place_manager_more_add -> {
                    true
                }
                R.id.place_manager_more_db -> {
                    val action =
                        PlaceManagerFragmentDirections.actionPlaceManagerFragmentToDbManagerFragment()
                    findNavController().navigate(action)
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }

        return binding.root
    }

    private fun createPlace() {
        val action =
            PlaceManagerFragmentDirections.actionPlaceManagerFragmentToPlaceEditFragment()
        action.placeId = ""
        action.loc = latestLocation
        findNavController().navigate(action)
    }

    private fun info() {
        val loc = latestLocation
        Toast.makeText(context, "Pos (${loc.latitude}, ${loc.longitude})", Toast.LENGTH_LONG).show()

        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        Timber.d("WAWA info $storageDir")
        storageDir?.let {
            storageDir.listFiles()?.let { fileArray ->
                for (file in fileArray) {
                    Timber.d("WAWA file $file")
                }
            }
        }
    }

    private fun initLocationService() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Timber.d("WAWA no loc permissions")
            return
        }
        val locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this.context, "GPS is off", Toast.LENGTH_LONG).show()
            val item = binding.placeManagerToolbar.menu.findItem(R.id.place_manager_more_refresh_pos)
            item.isEnabled = false
        }
        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)?.let {
            onLocationChanged(it)
        }
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            5000,
            10f,
            this
        )
    }

    override fun onLocationChanged(loc: Location) {
        latestLocation = Location(loc)
        val item = binding.placeManagerToolbar.menu.findItem(R.id.place_manager_more_refresh_pos)
        item.isEnabled = true
    }

    override fun onProviderDisabled(provider: String) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

}
