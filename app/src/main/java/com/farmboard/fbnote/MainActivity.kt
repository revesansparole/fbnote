package com.farmboard.fbnote

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import timber.log.Timber


class MainActivity : AppCompatActivity() {
    companion object {
        fun initLogging() {
            if (Timber.treeCount() != 0) return
            Timber.plant(Timber.DebugTree())
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLogging()

        setContentView(R.layout.activity_main)
    }


}

